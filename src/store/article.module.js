import Vue from "vue";
import {
    ArticlesService,
    TagsService
} from "@/common/api.service";
import {
    FETCH_ARTICLE,
    ARTICLE_PUBLISH,
    ARTICLE_EDIT,
    ARTICLE_EDIT_ADD_TAG,
    ARTICLE_EDIT_REMOVE_TAG,
    ARTICLE_RESET_STATE,
    FETCH_TAGS
} from "./actions.type";
import {
    RESET_STATE,
    SET_ARTICLE,
    TAG_ADD,
    TAG_REMOVE,
    SET_TAGS
} from "./mutations.type";

const initialState = {
    article: {
        author: {},
        title: "",
        description: "",
        body: "",
        tagList: []
    },
    tags: []
};

export const state = { ...initialState };

export const actions = {
    async [FETCH_ARTICLE](context, articleSlug, prevArticle) {
        if (prevArticle !== undefined) {
            return context.commit(SET_ARTICLE, prevArticle);
        }
        const { data } = await ArticlesService.get(articleSlug);
        context.commit(SET_ARTICLE, data.article);
        return data;
    },
    [ARTICLE_PUBLISH](context, article) {
        return ArticlesService.create(article);
    },
    [ARTICLE_EDIT]({ state }, article) {
        return ArticlesService.update(state.article.slug, article);
    },
    [ARTICLE_EDIT_ADD_TAG](context, tag) {
        context.commit(TAG_ADD, tag);
    },
    [ARTICLE_EDIT_REMOVE_TAG](context, tag) {
        context.commit(TAG_REMOVE, tag);
    },
    [ARTICLE_RESET_STATE]({ commit }) {
        commit(RESET_STATE);
    },
    async [FETCH_TAGS]({commit}) {
        const { data } = await TagsService.get();
        commit(SET_TAGS, data.tags);
        return data;
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [SET_ARTICLE](state, article) {
        state.article = article;
    },
    [TAG_ADD](state, tag) {
        state.article.tagList = state.article.tagList.concat([tag]);
    },
    [TAG_REMOVE](state, tag) {
        state.article.tagList = state.article.tagList.filter(t => t !== tag);
    },
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, initialState[f]);
        }
    },
    [SET_TAGS](state, tags) {
        state.tags = tags;
    },
};

const getters = {
    article(state) {
        return state.article;
    },
    comments(state) {
        return state.comments;
    },
    tags: (state) => state.tags
};

export default {
    state,
    actions,
    mutations,
    getters
};
