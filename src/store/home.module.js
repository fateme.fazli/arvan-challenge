import {ArticlesService} from "@/common/api.service";
import {FETCH_ARTICLES, ARTICLE_DELETE} from "./actions.type";
import {
    FETCH_START,
    FETCH_END
} from "./mutations.type";

const state = {
    articles: [],
    tags: [],
    isLoading: true,
    articlesCount: 0
};

const getters = {
    articlesCount(state) {
        return state.articlesCount;
    },
    articles(state) {
        return state.articles;
    },
    isLoading(state) {
        return state.isLoading;
    }
};

const actions = {
    [FETCH_ARTICLES]({commit}, params) {
        commit(FETCH_START);
        return ArticlesService.query("", params.filters)
            .then(({data}) => {
                commit(FETCH_END, data);
            })
            .catch(error => {
                throw new Error(error);
            });
    },
    [ARTICLE_DELETE](context, slug) {
        return ArticlesService.destroy(slug);
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
    [FETCH_START](state) {
        state.isLoading = true;
    },
    [FETCH_END](state, {articles, articlesCount}) {
        state.articles = articles;
        state.articlesCount = articlesCount;
        state.isLoading = false;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
