import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: "/",
            component: () => import("@/views/Home"),
            meta: {
                title: 'Home',
                auth: true
            },
            children: [
                {
                    path: "",
                    redirect: '/articles'
                },
                {
                    path: "articles",
                    name: "articles",
                    component: () => import("@/views/ArticleList"),
                    meta: {
                        title: 'All posts'
                    }
                },
                {
                    name: "article-edit",
                    path: "articles/edit/:slug?",
                    props: true,
                    component: () => import("@/views/ArticleEdit"),
                    meta: {
                        title: 'Edit Article'
                    }
                },
                {
                    name: "article-create",
                    path: "articles/create",
                    component: () => import("@/views/ArticleEdit"),
                    meta: {
                        title: 'New Article'
                    }
                }
            ]
        },
        {
            name: "register",
            path: "/register",
            meta: {
                title: 'Register',
                auth: false
            },
            component: () => import("@/views/Register")
        },
        {
            name: "login",
            path: "/login",
            meta: {
                title: 'Login',
                auth: false
            },
            component: () => import("@/views/Login")
        },
        { path: '*', redirect: '/' }
    ]
});