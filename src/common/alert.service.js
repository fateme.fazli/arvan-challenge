import {Subject} from "rxjs";

export default class AlertService {
    static shared = new AlertService();

    errors$;
    errorsStream$ = new Subject();
    static get instance() {
        return this.shared;
    }
    constructor() {
        this.errors$ = this.errorsStream$.asObservable();
    }
    onError(error) {
        const response = error.response;
        if (response && response.status >= 400 && response.status < 405) {
            return false;
        }
        this.errorsStream$.next({error , status: 'danger'});
    }
    onSuccess(message) {
        this.errorsStream$.next({message, status: 'success'});
    }
    listen() {
        return this.errors$;
    }
}