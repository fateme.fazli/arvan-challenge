import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import AuthService from "@/common/auth.service";
import {API_URL} from "@/common/config";
import AlertService from "@/common/alert.service"

export const METHOD_MAPPER = {
    delete: "delete",
    put: 'updated',
    post: 'create'
};

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
        Vue.axios.interceptors.response.use(response => {
            if (response.status === 200) {
                const method = response.config.method;
                if(['put', 'post', 'delete'].includes(method)) {
                    this.successHandler(method, response.config.url);
                }
                return response;
            }
            this.errorHandler(response.data);
        }, (error) => {
            if (error.response && error.response.data) {
                this.errorHandler(error.response.data);
            } else {
                this.errorHandler(error.message);
            }
            return Promise.reject(error);
        });
    },
    query(resource, params) {
        return Vue.axios.get(resource, params).catch(error => {
            throw new Error(`ApiService ${error}`);
        });
    },
    setHeader() {
        Vue.axios.defaults.headers.common[
            "Authorization"
            ] = `Token ${AuthService.getToken()}`;
    },
    get(resource, slug = "") {
        return Vue.axios.get(`${resource}/${slug}`).catch(error => {
            throw new Error(`ApiService ${error}`);
        });
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },

    delete(resource) {
        return Vue.axios.delete(resource).catch(error => {
            throw new Error(`ApiService ${error}`);
        });
    },
    update(resource, slug, params) {
        return Vue.axios.put(`${resource}/${slug}`, params);
    },

    put(resource, params) {
        return Vue.axios.put(`${resource}`, params);
    },
    errorHandler(error) {
        AlertService.instance.onError(error);
    },
    successHandler(method, url) {
        const api = new URL(url).pathname.split('/')[2];
        let message = null;
        if (api !== 'users') {
            message = api + ' has been ' + METHOD_MAPPER[method] + 'ed';
        }
        AlertService.instance.onSuccess(message);
    }
};

export default ApiService;

export const ArticlesService = {
    query(type, params) {
        return ApiService.query("articles" + (type === "feed" ? "/feed" : ""), {
            params: params
        });
    },
    get(slug) {
        return ApiService.get("articles", slug);
    },
    create(article) {
        return ApiService.post("articles", {article});
    },
    update(slug, article) {
        return ApiService.update("articles", slug, {article});
    },
    destroy(slug) {
        return ApiService.delete(`articles/${slug}`);
    }
};

export const TagsService = {
    get() {
        return ApiService.get("tags");
    }
};

