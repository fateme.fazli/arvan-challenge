import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store";
import ApiService from "./common/api.service";
import VeeValidate from 'vee-validate';
import { CHECK_AUTH } from "./store/actions.type";
import BootstrapVue from 'bootstrap-vue'
import VueRx from 'vue-rx'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;
ApiService.init();
Vue.use(VeeValidate,  { fieldsBagName: 'veeFields' });
Vue.use(BootstrapVue);
Vue.use(VueRx);
router.beforeEach(({name}, from, next) => {
    if (['login', 'register'].includes(name)) {
        next();
    } else {
        return Promise.all([store.dispatch(CHECK_AUTH)]).then(next).catch(() => {
            router.push({
                name: 'login'
            })
        });
    }
});

const render = h => h(App);

new Vue({
    router,
    store,
    render
}).$mount('#app');
