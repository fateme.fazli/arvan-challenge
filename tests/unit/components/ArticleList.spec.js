import { mount, shallowMount } from "@vue/test-utils";
import Vue from "vue";
import Vuex from "vuex";

import ArticleList from "../../../src/views/ArticleList.vue";
import {
    FETCH_ARTICLES,
    ARTICLE_DELETE
} from "../../../src/store/actions.type";

Vue.use(Vuex);
const initialState = {
    articles: [],
    tags: [],
    isLoading: true,
    articlesCount: 0
};
const getters = {
    articlesCount(state) {
        return state.articlesCount;
    },
    articles(state) {
        return state.articles;
    },
    isLoading(state) {
        return state.isLoading;
    }
};

describe("ArticleList", () => {
    let store = null;

    beforeEach(() => {
        const actions = {
            [FETCH_ARTICLES]: jest.fn(),
            [ARTICLE_DELETE]: jest.fn()
        };
        store = new Vuex.Store({
            actions,
            getters,
            state: initialState
        });
    });

    it('has a created hook', () => {
        expect(typeof ArticleList.created).toBe('function');
    });

    it('sets the correct default data', () => {
        expect(typeof ArticleList.data).toBe('function');
        const defaultData = ArticleList.data();
        expect(defaultData.currentPage).toBe(1);
        expect(defaultData.fields).toStrictEqual([
            {key: 'index', label: "#"},
            'title',
            'author',
            {key: 'tagList', label: 'Tags'},
            {key: 'body', label: 'Excerpt'},
            {key: 'createdAt', label: 'Created'},
            {key: 'slug', label: ''}
        ]);
        expect(defaultData.selectedArticleSlug).toBe('');
    });

    it('renders correctly with different props', () => {
        const itemsPerPage = 20;
        const component = shallowMount(ArticleList, { store });
        component.setProps({itemsPerPage});
        const listConfig = component.vm.listConfig;
        expect(listConfig).toStrictEqual({
            filters: {
                offset: 0,
                limit: itemsPerPage
            }
        });
    });
});