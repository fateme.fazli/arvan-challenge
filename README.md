# Arvan-challenge
This is [Arvancloud](https://github.com/arvancloud) front-end interview challenge project that was created to demonstrate a basic article management application built with Vue.js including CRUD operations, authentication, routing, pagination, and more.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run unit tests
```
npm run test
```

### To know
   Current arbitrary choices are:
   
   - [Vuex](https://github.com/vuejs/vuex) modules for store
   - [Vue-axios](https://github.com/imcvampire/vue-axios) for ajax requests
   - [Bootstrap-vue](https://github.com/bootstrap-vue/bootstrap-vue) components and grid system for UI 
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### License
   This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/fateme.fazli/arvan-challenge/blob/master/README.md) file for details.